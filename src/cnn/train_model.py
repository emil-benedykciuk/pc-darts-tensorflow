import tensorflow as tf
from absl import app

from src.cnn.utils.flags import *
from src.cnn.utils.network import GeneratedNetworkCIFAR
from src.cnn.utils.trainer import Trainer
from src.cnn.utils.dataset.consts import EVAL
from src.cnn.utils.dataset.dataset_reader import ImageDatasetReader

FLAGS = flags.FLAGS
tf.get_logger().setLevel("INFO")

physical_devices = tf.config.list_physical_devices('GPU')
if len(physical_devices) > 0:
    for device in physical_devices:
        tf.config.experimental.set_memory_growth(device, True)


def train(_):
    loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=FLAGS.learning_rate)
    train_data_reader = ImageDatasetReader(
        batch_size=FLAGS.batch_size,
        epochs=FLAGS.epochs,
        tfrecords_path=FLAGS.tfrecords_path,
        num_classes=FLAGS.num_classes
    )
    eval_data_reader = ImageDatasetReader(
        batch_size=FLAGS.batch_size,
        epochs=FLAGS.epochs,
        tfrecords_path=FLAGS.tfrecords_path,
        mode=EVAL,
        num_classes=FLAGS.num_classes
    )
    network = GeneratedNetworkCIFAR(
        num_filters=FLAGS.num_filters,
        num_filters_multiplier=FLAGS.num_filters_multiplier,
        num_layers=FLAGS.num_layers,
        num_classes=FLAGS.num_classes,
        input_shape=(32, 32, 3),
        cells_genotype=FLAGS.genotype
    )
    trainer = Trainer(
        num_epochs=FLAGS.epochs,
        network=network,
        optimizer=optimizer,
        loss_fn=loss_fn,
        results_path=FLAGS.results_path
    )
    trainer.train(
        train_dataset_reader=train_data_reader,
        validation_dataset_reader=eval_data_reader
    )


if __name__ == '__main__':
    app.run(train)
