import tensorflow as tf

LOG = "LOG"
INFO = "INFO"
DEBUG = "DEBUG"
ERROR = "ERROR"
FATAL = "FATAL"

tf.get_logger().setLevel(INFO)


def int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def int64_list_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def bytes_list_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def float_list_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def print_msg(msg, msg_type=None):
    if msg_type == LOG:
        tf.compat.v1.logging.log(msg)
    elif msg_type == DEBUG:
        tf.compat.v1.logging.debug(msg)
    elif msg_type == ERROR:
        tf.compat.v1.logging.error(msg)
    elif msg_type == FATAL:
        tf.compat.v1.logging.fatal(msg)
    else:
        tf.compat.v1.logging.info(msg)
