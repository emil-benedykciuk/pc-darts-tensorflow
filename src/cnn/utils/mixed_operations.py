import tensorflow as tf

from src.cnn.utils.operations.utils import channel_shuffle
from src.cnn.utils.operations.consts import PRIMITIVES, CHANNEL_PROPORTION_COEFFICIENT
from src.cnn.utils.operations.modules import DARTS_OPS, IdentityModule


class MixedOp(tf.keras.layers.Layer):
    def __init__(self, num_filters, stride, name=None, **kwargs):
        super(MixedOp, self).__init__(name=name, **kwargs)
        self.ops = None
        self.stride = stride
        self.num_filters = num_filters
        self.k = CHANNEL_PROPORTION_COEFFICIENT

        self._init_ops()

    def _init_ops(self):
        if self.stride == 2:
            self.channel_proportion_op = tf.keras.layers.MaxPooling2D(
                pool_size=(2, 2),
                strides=(2, 2),
                padding='valid',
                name=self.name + "_pc_extra_max_pool"
            )
        else:
            self.channel_proportion_op = IdentityModule(
                name=self.name + "_pc_extra_identity"
            )
        self.ops = []
        for primitive in PRIMITIVES:
            self.ops.append(DARTS_OPS[primitive](
                num_filters=self.num_filters // self.k,
                stride=self.stride,
                name=self.name + '_' + str(primitive)
            ))

    def call(self, inputs, *args, **kwargs):
        x, alphas_weights = inputs
        x_1 = x[:, :, :, :x.shape[-1] // self.k]
        x_2 = x[:, :, :, x.shape[-1] // self.k:]

        x_1 = tf.add_n([w * op(x_1) for w, op in zip(tf.split(alphas_weights, len(PRIMITIVES)), self.ops)])
        result = tf.concat([x_1, self.channel_proportion_op(x_2)], axis=3)
        return channel_shuffle(result, self.k)
