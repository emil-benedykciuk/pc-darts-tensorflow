import tensorflow as tf


class DatasetGenerator(tf.keras.utils.Sequence):
    def __init__(self, dataset=None, limitless_dataset=True):
        self._dataset = dataset(limitless_dataset=limitless_dataset, prefetch=True)
        self._iterator = self._dataset.__iter__()
        self._len = len(dataset) // dataset.batch_size

    def __getitem__(self, item):
        image, label = self._iterator.get_next()
        return image, label

    def __len__(self):
        return self._len
