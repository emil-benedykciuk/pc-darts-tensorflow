import os

import tensorflow as tf

from src.cnn.utils.dataset.consts import TRAIN, TFRECORDS_PATH, TFRecordFields as Fields
from src.cnn.utils.dataset.preprocessing import cifar_10_image_preprocessing


class ImageDatasetReader:
    def __init__(self, batch_size, epochs, tfrecords_path=None, img_pre_fn=None, mode=TRAIN, num_classes=10):
        self.num_classes = num_classes
        self.tfrecords_path = tfrecords_path or TFRECORDS_PATH
        self.batch_size = batch_size
        self.epochs = epochs
        self._img_preprocessing_fn = img_pre_fn or cifar_10_image_preprocessing

        self._paths = [os.path.join(self.tfrecords_path, mode + '.tfrecords'), ]
        self._num_records = self._get_number_of_records()
        self._keys_to_features = {
            Fields.height: tf.io.FixedLenFeature([], tf.int64),
            Fields.width: tf.io.FixedLenFeature([], tf.int64),
            Fields.channels: tf.io.FixedLenFeature([], tf.int64),
            Fields.classes: tf.io.FixedLenFeature([], tf.int64),
            Fields.label: tf.io.FixedLenFeature([], tf.int64),
            Fields.filename: tf.io.FixedLenFeature([], tf.string),
            Fields.source_id: tf.io.FixedLenFeature([], tf.string),
            Fields.image: tf.io.FixedLenFeature([], tf.string)
        }

    def _get_number_of_records(self):
        return sum([1 for _ in tf.data.TFRecordDataset(self._paths)])

    @tf.autograph.experimental.do_not_convert
    def _process_example(self, proto):
        parsed_features = tf.io.parse_single_example(proto, self._keys_to_features)
        image_bytes = tf.reshape(parsed_features[Fields.image], shape=[])
        label = tf.one_hot([parsed_features[Fields.label]], self.num_classes)
        image = self._img_preprocessing_fn(image_bytes=image_bytes)
        return image, label[0]

    def __len__(self):
        return self._num_records

    def __call__(self, shuffle=True, limitless_dataset=True, take=None, prefetch=True):
        dataset = tf.data.TFRecordDataset(self._paths)
        dataset = dataset.map(self._process_example, num_parallel_calls=tf.data.experimental.AUTOTUNE)

        if shuffle:
            dataset = dataset.shuffle(self.batch_size)

        if take:
            return dataset.take(take)
        else:
            dataset = dataset.batch(self.batch_size)

        if limitless_dataset:
            dataset = dataset.repeat(self.epochs)

        if prefetch:
            dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

        return dataset
