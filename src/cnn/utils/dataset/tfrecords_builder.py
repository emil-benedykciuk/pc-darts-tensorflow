import os
import pickle
import random

import numpy as np
import tensorflow as tf
import src.cnn.utils.tf_utils as tf_utils

from src.cnn.utils.progress_bar import SimpleProgressBar
from src.cnn.utils.dataset.consts import TRAIN, EVAL, DATA_PATH, BUILD_RESULTS_PATH, TFRecordFields as Fields


class ImageDatasetBuilder:
    def __init__(self, num_classes, data_path=None, result_path=None, validation_part=0.2, generate_from_images=False,
                 cifar_10=True):
        self.data_path = data_path or DATA_PATH
        self.result_path = result_path or BUILD_RESULTS_PATH
        self.num_classes = num_classes
        self.validation_part = validation_part
        self.generate_from_images = generate_from_images
        self.cifar_10 = cifar_10

        self._dataset = {
            TRAIN: [],
            EVAL: []
        }

        if self.generate_from_images:
            self._init_data_list()
            self._init_split_data()
        elif self.cifar_10:
            self._init_cifar_10_data_list()
            self._init_split_cifar_10_data()

    @staticmethod
    def _unpickle(path):
        with open(path, 'rb') as f:
            data = pickle.load(f, encoding='bytes')
        return data

    def _init_cifar_10_data_list(self):
        meta_path = os.path.join(self.data_path, 'batches.meta')
        meta_dict = ImageDatasetBuilder._unpickle(meta_path)
        self._cifar_labels_names = np.array(meta_dict[b'label_names'])
        self.num_classes = self._cifar_labels_names.shape[0]

        data = None
        labels = []
        filenames = []
        data_paths = [os.path.join(self.data_path, "data_batch_{}".format(i)) for i in range(1, 6)]
        for idx, path in enumerate(data_paths):
            data_dict = ImageDatasetBuilder._unpickle(path)
            if not idx:
                data = data_dict[b'data']
            else:
                data = np.vstack((data, data_dict[b'data']))
            labels += data_dict[b'labels']
            filenames += data_dict[b'filenames']

        data = data.reshape((len(data), 3, 32, 32))
        data = np.rollaxis(data, 1, 4)
        labels = np.array(labels)
        filenames = np.array(filenames)

        self._cifar_dataset = []
        for i in range(data.shape[0]):
            self._cifar_dataset.append((data[i], labels[i], filenames[i].decode("utf-8")))

    def _init_split_cifar_10_data(self):
        num_images = len(self._cifar_dataset)
        num_val_images = int(self.validation_part * num_images)
        self._dataset[TRAIN].extend(self._cifar_dataset[:-num_val_images])
        self._dataset[EVAL].extend(self._cifar_dataset[-num_val_images:])

    def _init_data_list(self):
        tf_utils.print_msg("Collecting images names to process.")
        self._class_images_names = {str(c): sorted([x for x in os.listdir(os.path.join(self.data_path, str(c))) if
                                                    os.path.isdir(os.path.join(self.data_path, x))]) for c in
                                    range(self.num_classes)}

    def _init_split_data(self):
        tf_utils.print_msg("Dividing the images names into training and validation datasets.")
        for c in range(self.num_classes):
            num_images = len(self._class_images_names[str(c)])
            num_val_images = int(self.validation_part * num_images)
            random.shuffle(self._class_images_names[str(c)])
            self._dataset[TRAIN].extend([(c, name) for name in self._class_images_names[str(c)][:-num_val_images]])
            self._dataset[EVAL].extend([(c, name) for name in self._class_images_names[str(c)][-num_val_images:]])
        random.shuffle(self._dataset[TRAIN])
        random.shuffle(self._dataset[EVAL])

    def _get_image(self, label, image_name):
        path = os.path.join(self.data_path, label, image_name)
        with tf.io.gfile.GFile(path, 'rb') as f:
            image_data = f.read()
        img = tf.image.decode_jpeg(image_data)

        height = img.shape[0]
        width = img.shape[1]
        channels = img.shape[2]
        return image_data, height, width, channels

    @staticmethod
    def _create_tf_example(image_bytes, label, filename, num_classes, source_id, height, width, channels):
        features = {
            Fields.height:
                tf_utils.int64_feature(height),
            Fields.width:
                tf_utils.int64_feature(width),
            Fields.channels:
                tf_utils.int64_feature(channels),
            Fields.classes:
                tf_utils.int64_feature(num_classes),
            Fields.label:
                tf_utils.int64_feature(label),
            Fields.filename:
                tf_utils.bytes_feature(str(filename).encode('utf8')),
            Fields.source_id:
                tf_utils.bytes_feature(str(source_id).encode('utf8')),
            Fields.image:
                tf_utils.bytes_feature(image_bytes),
        }
        example = tf.train.Example(features=tf.train.Features(feature=features))
        return example

    def _create_tfrecords_from_images(self):
        for key, names in self._dataset.items():
            prog = SimpleProgressBar(name=key.upper(), total=len(names))
            filename = os.path.join(self.result_path, key + '.tfrecords')
            writer = tf.io.TFRecordWriter(filename)
            for (label, image_name) in names:
                image_data, height, width, channels = self._get_image(label, image_name)
                example = ImageDatasetBuilder._create_tf_example(
                    image_bytes=image_data,
                    label=label,
                    filename=image_name,
                    num_classes=self.num_classes,
                    source_id=image_name.split('.')[0],
                    height=height,
                    width=width,
                    channels=channels
                )
                writer.write(example.SerializeToString())
                prog.next()
            prog.end()
            tf_utils.print_msg("The %s tfrecord has been created: %s" % (key, filename))
            writer.close()

    def _create_tfrecords_from_cifar_10(self):
        for key, records in self._dataset.items():
            prog = SimpleProgressBar(name=key.upper(), total=len(records))
            filename = os.path.join(self.result_path, key + '.tfrecords')
            writer = tf.io.TFRecordWriter(filename)
            for (image_data, label, image_name) in records:
                example = ImageDatasetBuilder._create_tf_example(
                    image_bytes=image_data.tobytes(),
                    label=label,
                    filename=image_name,
                    num_classes=self.num_classes,
                    source_id=image_name.split('.')[0],
                    height=32,
                    width=32,
                    channels=3
                )
                writer.write(example.SerializeToString())
                prog.next()
            prog.end()
            tf_utils.print_msg("The %s tfrecord has been created: %s" % (key, filename))
            writer.close()

    def __call__(self):
        tf_utils.print_msg("Creating tfrecords files...")
        if self.generate_from_images:
            self._create_tfrecords_from_images()
        elif self.cifar_10:
            self._create_tfrecords_from_cifar_10()
