import tensorflow as tf


@tf.function
def cifar_10_image_preprocessing(image_bytes):
    image = tf.io.decode_raw(image_bytes, tf.uint8)
    image = tf.reshape(image, [32, 32, 3])
    return image


@tf.function
def base_image_preprocessing(image_bytes):
    image = tf.io.decode_jpeg(image_bytes)
    return image
