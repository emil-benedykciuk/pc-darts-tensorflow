from absl import flags

flags.DEFINE_float('learning_rate', 0.025,
                   "The learning rate value for optimizer.")
flags.DEFINE_float('min_learning_rate', 0.001,
                   "The minimal learning rate value for optimizer.")
flags.DEFINE_float('momentum', 0.9,
                   "The momentum value for optimizer.")
flags.DEFINE_float('weight_decay', 3e-4,
                   "The weight_decay value for optimizer.")
flags.DEFINE_integer('batch_size', 64,
                     "The size of data batch for train.")
flags.DEFINE_integer('epochs', 100,
                     "The number of training epochs.")
flags.DEFINE_integer('num_filters', 16,
                     "The number of filters on first cell.")
flags.DEFINE_integer('num_filters_multiplier', 3,
                     "The value that represents the multiplier for the number of filters per next cell.")
flags.DEFINE_integer('num_layers', 8,
                     "The number of cells that make up the network.")
flags.DEFINE_integer('num_nodes', 4,
                     "The number of nodes in cell.")
flags.DEFINE_integer('num_reduction_nodes', 4,
                     "The number of node's cell that make up (by concatenation) the output for this cell.")
flags.DEFINE_integer('num_classes', 10,
                     "The number of classes for the model being trained.")
flags.DEFINE_string('data_path', '/data/',
                    "The path to directory, where train and eval tfrecords are stored.")
flags.DEFINE_string('tfrecords_path', '/tfrecords/',
                    "The path to directory, where train and eval tfrecords are stored.")
flags.DEFINE_string('results_path', '/results/',
                    "The path to directory, where training data: checkpoints, tensorboard stats will be saved.")
flags.DEFINE_string('genotype', None,
                    "The genotype of generated cells. Cells format must match this implementation format.")

