import tensorflow as tf


def channel_shuffle(x, groups):
    _, h, w, c = x.shape

    num_channels_per_group = c // groups
    x = tf.reshape(x, [-1, h, w, groups, num_channels_per_group])
    x = tf.transpose(x, [0, 1, 2, 4, 3])
    x = tf.reshape(x, [-1, h, w, c])
    return x
