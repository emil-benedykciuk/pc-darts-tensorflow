import os

import tensorflow as tf

from src.cnn.utils.dataset.dataset_generator import DatasetGenerator


class Trainer:
    def __init__(self, num_epochs, network, optimizer, loss_fn, results_path):
        self.num_epochs = num_epochs
        self.network = network
        self.optimizer = optimizer
        self.loss_fn = loss_fn
        self.results_path = results_path

    def _get_callbacks_list(self):
        tb_path = os.path.join(self.results_path, 'tensorboard_logs')
        tb = tf.keras.callbacks.TensorBoard(
            log_dir=tb_path,
            write_graph=True,
            update_freq='epoch'
        )
        mc = tf.keras.callbacks.ModelCheckpoint(
            mode='min',
            filepath=os.path.join(self.results_path, '{val_loss:.3f}_{epoch:02d}_weights.h5'),
            monitor='val_loss',
            save_best_only=False,
            save_weights_only=True,
            verbose=1,
            save_freq='epoch'
        )
        return [tb, mc]

    def train(self, train_dataset_reader, validation_dataset_reader):
        train_data_generator = DatasetGenerator(dataset=train_dataset_reader)
        eval_data_generator = DatasetGenerator(dataset=validation_dataset_reader)

        self.network.model.compile(optimizer=self.optimizer, loss=self.loss_fn, metrics=['accuracy'])
        self.network.model.fit_generator(
            generator=train_data_generator,
            epochs=self.num_epochs,
            validation_data=eval_data_generator,
            callbacks=self._get_callbacks_list()
        )
