import tensorflow as tf

from absl import app

from src.cnn.utils.flags import *
from src.cnn.utils.dataset.tfrecords_builder import ImageDatasetBuilder

FLAGS = flags.FLAGS
tf.get_logger().setLevel("INFO")


def build(_):
    builder = ImageDatasetBuilder(
        num_classes=10,
        data_path=FLAGS.data_path,
        result_path=FLAGS.tfrecords_path,
        validation_part=0.2,
        generate_from_images=False,
        cifar_10=True)
    builder()


if __name__ == '__main__':
    app.run(build)
